package lekgin

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func BuildResult(c *gin.Context, data interface{}) {
	c.JSON(http.StatusOK, gin.H{
		"code":   http.StatusOK,
		"status": true,
		"data":   data,
	})
}

func BuildResultInternalServerError(c *gin.Context) {
	c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
		"code":    http.StatusInternalServerError,
		"status":  false,
		"message": "Internal server error!",
	})
}

func BuildResultError(c *gin.Context, code int, s string) {
	c.AbortWithStatusJSON(code, gin.H{
		"code":    code,
		"status":  false,
		"message": s,
	})
}
